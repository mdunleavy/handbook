We have a [metrics dashboard](https://app.periscopedata.com/app/gitlab/561630/Dev-Sub-department-Overview-Dashboard) intended to
track against some of the [Development Department KPIs](/handbook/company/kpis/#development-department-kpis), particularly
those around merge request creation and acceptance. From that dashboard, the
following charts shows [MR Rate](/handbook/engineering/development/performance-indicators/#mr-rate).

{{% sisense dashboard="561630" chart="7415633" %}}

The following chart shows the MR Rate of the Dev section as a whole, for the
identification of trends:

{{% sisense dashboard="561630" chart="7305986" %}}
