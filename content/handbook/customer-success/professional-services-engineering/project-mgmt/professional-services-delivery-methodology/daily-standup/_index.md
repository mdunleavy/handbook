---
title: "Daily Standup"
description: "Learn about Daily Standups."
---

The Daily Stand Up meeting is a brief, tightly facilitated status and risk management meeting for all team members, led by the Program Manager / Project Manager.

The duration is short, targeted at 15 minutes, but no more than 30 minutes.

The main purpose of the daily standup is to facilitate a conversation around meeting the sprint commitment.

It is to raise risks and issues, some of which may be blockers, and to identify if other meetings or sessions need to be held either with all team members or just a couple.

Three questions each participant should ask / answer:

1. What did you accomplish yesterday towards the Iteration goal?
2. What do you plan to accomplish today towards the Iteration goal?
3. What can the team help you with?

One thing to emphasize is that it is not for problem solving - no solutioning!

It is to raise a problem that exists and then to schedule time to explore it later. And that’s why we time box it to about 15 minutes because we want it to be quick and not waste anyone’s time.

Guidelines:

* Reference specific tasks (at the in the engagement board if possible)
* Record & Review Risks and Issues
* Team members should speak to one another; this is an alignment tool, not a reporting exercise
